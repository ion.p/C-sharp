﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace learning
{
    class Roads
    {
        Model model;
        Vector3 pos;
        Matrix[] bonesTransformation;

        public Roads()
        {
           // box2 = new BoundingBox(new Vector3(pos.X - size / 2, pos.Y, pos.Z - size / 2), new Vector3(pos.X + size / 2, pos.Y + size, pos.Z + size / 2));
        }

        public void loadContent(ContentManager content)
        {
            model = content.Load<Model>("roads");
            pos = new Vector3(0, -65, 0);
        }
        public void draw(Matrix projection, Matrix view)
        {
            bonesTransformation = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(bonesTransformation);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.TextureEnabled = false;
                    effect.World = bonesTransformation[mesh.ParentBone.Index] * Matrix.CreateScale(1f) * Matrix.CreateTranslation(pos);
                    effect.View = view;
                    effect.Projection = projection;
                    effect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }
    }
}
