﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace learning
{
    class Trees
    {
        Model model;
        Vector3 posTrees;
        Matrix[] bonesTransformation;
        public static VertexPositionTexture[] vertices;
        public static BoundingSphere boundingSphere;

        public Trees() {
            vertices = new VertexPositionTexture[28];
            //first sqaure
            vertices[0].Position = new Vector3(-50, -20, 400);
            vertices[1].Position = new Vector3(150, -20, 600);
            vertices[2].Position = new Vector3(100, -20, 250);
            vertices[3].Position = new Vector3(500, -20, 1000); 
            vertices[4].Position = new Vector3(700, -20, 100);
            vertices[5].Position = new Vector3(100, -20, 760); 
            vertices[6].Position = new Vector3(300, -20, 100);
            //second square
            vertices[7].Position = new Vector3(-50, -20, -700);
            vertices[8].Position = new Vector3(150, -20, -900);
            vertices[9].Position = new Vector3(100, -20, -550);
            vertices[10].Position = new Vector3(500, -20, -1300);
            vertices[11].Position = new Vector3(700, -20, -300);
            vertices[12].Position = new Vector3(100, -20, -960);
            vertices[13].Position = new Vector3(300, -20, -300);
            //third square
            vertices[14].Position = new Vector3(-1050, -20, -700);
            vertices[15].Position = new Vector3(-1250, -20, -900);
            vertices[16].Position = new Vector3(-1300, -20, -550);
            vertices[17].Position = new Vector3(-1100, -20, -1300);
            vertices[18].Position = new Vector3(-1300, -20, -300);
            vertices[19].Position = new Vector3(-800, -20, -960);
            vertices[20].Position = new Vector3(-900, -20, -300);
            //forth square
            vertices[21].Position = new Vector3(-1050, -20, 1400);
            vertices[22].Position = new Vector3(-1250, -20, 1500);
            vertices[23].Position = new Vector3(-1300, -20, 1050);
            vertices[24].Position = new Vector3(-1100, -20, 800);
            vertices[25].Position = new Vector3(-1300, -20, 1300);
            vertices[26].Position = new Vector3(-800, -20, 960);
            vertices[27].Position = new Vector3(-900, -20, 1100);
            for (int t = 0; t < vertices.Length; t++)
            {
                boundingSphere.Radius = 40f;
                boundingSphere.Center = vertices[t].Position;
            } 
        }
        public void loadContent(ContentManager content)
        {
            model = content.Load<Model>("tree");
        }
        public void draw(Matrix projection, Matrix view)
        {
            for (int h = 0; h < vertices.Length; h++)
            {
                bonesTransformation = new Matrix[model.Bones.Count];
                model.CopyAbsoluteBoneTransformsTo(bonesTransformation);
                foreach (ModelMesh mesh in model.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.TextureEnabled = false;
                        effect.World = bonesTransformation[mesh.ParentBone.Index] * Matrix.CreateScale(1f) * Matrix.CreateTranslation(vertices[h].Position);
                        effect.View = view;
                        effect.Projection = projection;
                        effect.EnableDefaultLighting();
                    }
                    mesh.Draw();
                }
            }
        }

    }
}
