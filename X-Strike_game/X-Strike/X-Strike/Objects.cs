﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace learning
{
    class Objects
    { 
        //Vector3 pos;
        Matrix[] bonesTransformation;

        public List<House> houses = new List<House>(12);
        public List<Trees> trees = new List<Trees>(28);
        public List<Robot> robots = new List<Robot>(5);


        public BoundingSphere boundingSphere;

        public void draw(Matrix projection, Matrix view, Model model,Vector3 pos)
        {
            bonesTransformation = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(bonesTransformation);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.TextureEnabled = false;
                    effect.World = bonesTransformation[mesh.ParentBone.Index] * Matrix.CreateScale(1f) * Matrix.CreateTranslation(pos);
                    effect.View = view;
                    effect.Projection = projection;
                    effect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }

    }
}
