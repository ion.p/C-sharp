﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace learning
{
    class Robot:Objects
    {
        public Model model;
        public static Vector3 rotation;
        public Matrix view;
        Matrix[] bonesTransformation;
        public static Vector3 position, posR, posP;
        public const int robotsNum = 10;
        Vector3[] numRobots = new Vector3[robotsNum];

        public static VertexPositionTexture[] vertices;

        public float radius = 15f;
        public Vector3 Position;

        public Robot()
        {
            Position = Vector3.Zero;
            vertices = new VertexPositionTexture[5];

            vertices[0].Position = new Vector3(1000, 1, 30);
            vertices[1].Position = new Vector3(-1000, 1, 330);
            vertices[2].Position = new Vector3(500, 1, 230);
            vertices[3].Position = new Vector3(-500, 1, 130);
            vertices[4].Position = new Vector3(-850, 1, -830);
        }
        //
        public void loadContent(ContentManager content)
        {
            model = content.Load<Model>("robotball");
            boundingSphere.Radius = radius;
            boundingSphere.Center = position;
          //  

        } 
        public void Update()
        {
          //  RobotAttacking();
            UpdateRobotAnimation();

        }
        public void UpdateRobotAnimation()
        {
            // rotation.X += 1f;
            // rotation.Y += 1f;
            //rotation.Z += 1f;
        }

        //
        public void draw(Matrix projection, Matrix view)
        {
            for (int i = 0; i < vertices.Length; i++)
            {
                bonesTransformation = new Matrix[model.Bones.Count];
                model.CopyAbsoluteBoneTransformsTo(bonesTransformation);
                foreach (ModelMesh mesh in model.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.TextureEnabled = false;
                        effect.World = bonesTransformation[mesh.ParentBone.Index] * Matrix.CreateScale(1f) * Matrix.CreateRotationX(0)
                            * Matrix.CreateRotationY(rotation.Y) * Matrix.CreateRotationZ(rotation.Z) * Matrix.CreateTranslation(vertices[i].Position);
                        effect.View = view;
                        effect.Projection = projection;
                        effect.EnableDefaultLighting();
                    }
                    mesh.Draw();
                }
            }
        }//

        public BoundingSphere boundingBox { get; set; }
    }
}
