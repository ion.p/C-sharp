using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace learning
{
    public class XStrike : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        Matrix projection;
        Model planeModel, playerModel, bulletModel;
        Vector3 planePosition, bulletPosition;
        public enum GameState{ START, PREPARELEVEL, PLAYING, GAMEOVER, PAUSE, SECONDLEVEL, OPTIONS, HELP }
        GameState gameState = GameState.START;
        SpriteBatch spriteBatch;
        SpriteFont lucidaConsole;
        int points = 0;
        int lives = 3;

        // Audio objects
        Song soundEffect;
        SoundEffect shooting;
        SoundEffect game_over_sound;
        SoundEffect pause;
        SoundEffect Points;
        SoundEffect nextLevel;

        House house = new House();
        Roads roads = new Roads();
        Robot robot = new Robot();
        Robot robot2 = new Robot();
        Robot robot3 = new Robot();
        Robot robot4 = new Robot();
        Robot robot5 = new Robot();
        Trees tree = new Trees();
        Objects objects = new Objects();
        BoundingSphere boundingSphere, boundingShpereBullet;

        public static Vector3 pos;
        public float velocity = 5, dx, dz;

        public static Vector3 rotation, bulletRotation;
        Vector3 cameraPos;
        Vector3 lookAt;

        public Matrix view;
        Matrix[] bonesTransformation;
        Vector3 position;
        float robotsSpeed = 0.5f;
        float bulletSpeed = 150f;
        bool mute = true;
        float counter = 60;
        //array list 

        public XStrike()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;//set the cursor visible

            graphics.PreferredBackBufferWidth = 953;
            graphics.PreferredBackBufferHeight = 580;
            graphics.ApplyChanges();
            graphics.IsFullScreen = false;
            Window.Title = "X Strike!";

            position = Vector3.Zero;
            cameraPos = new Vector3(position.X, position.Y+12,position.Z);
            pos = position;
            bulletPosition = Vector3.Zero;
            boundingSphere.Radius = 20f;
            boundingSphere.Center = pos;

            boundingShpereBullet.Radius = 8f;
            boundingShpereBullet.Center = bulletPosition;
        }

        protected override void Initialize()
        {
            spriteBatch = new SpriteBatch(graphics.GraphicsDevice);
            robot.boundingSphere.Center = robot.Position;
            robot2.boundingSphere.Center = robot2.Position;
            robot3.boundingSphere.Center = robot3.Position;
            robot4.boundingSphere.Center = robot4.Position;
            robot5.boundingSphere.Center = robot5.Position;

            objects.houses.Add(house);
            objects.trees.Add(tree);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            lucidaConsole = Content.Load<SpriteFont>("Fonts/Lucida Console");

            soundEffect = Content.Load<Song>("Sound/Song1");
            shooting = Content.Load<SoundEffect>("Sound/shooting");
            pause = Content.Load<SoundEffect>("Sound/pause");
            nextLevel = Content.Load<SoundEffect>("Sound/pause");
            game_over_sound = Content.Load<SoundEffect>("Sound/game_over");

            planeModel = Content.Load<Model>("SC Private 001");
            playerModel = Content.Load<Model>("Gunman");
            bulletModel = Content.Load<Model>("bullet2");
           
            projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45f), graphics.GraphicsDevice.Viewport.AspectRatio,.1f,1000f);
            for (int h = 0; h < objects.houses.Count; h++) { objects.houses[h].loadContent(Content); }
            for (int t = 0; t < objects.trees.Count; t++) { objects.trees[t].loadContent(Content); }
           
            roads.loadContent(Content);
           // for (int i = 0; i < robots.Count; i++){robots[i].loadContent(Content);}     
            robot.loadContent(Content);
            robot2.loadContent(Content);
            robot3.loadContent(Content);
            robot4.loadContent(Content);
            robot5.loadContent(Content);
        }
        protected override void UnloadContent()
        {
            Content.Unload();
        }
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
                // Get the game pad state.
                GamePadState joyState = GamePad.GetState(PlayerIndex.One);
                // get keyboard state
                KeyboardState keyState = Keyboard.GetState();

                boundingSphere.Center = pos;
                boundingShpereBullet.Center = bulletPosition;

                //start game
                if (gameState == GameState.START)
                {
                    //MediaPlayer.Play(soundEffect);
                    // MediaPlayer.IsRepeating = true;
                    if (joyState.Buttons.Start == ButtonState.Pressed || keyState.IsKeyDown(Keys.Enter))
                    {
                        gameState = GameState.PLAYING;
                    }
                    if(joyState.Buttons.Back == ButtonState.Pressed || keyState.IsKeyDown(Keys.Q)){
                        this.Exit();
                    }
                    if (joyState.Triggers.Left == 1 || keyState.IsKeyDown(Keys.O))
                    {
                        gameState = GameState.OPTIONS;
                    }
                    if (keyState.IsKeyDown(Keys.H))
                    {
                        gameState = GameState.HELP;
                    }
                   //game help

                }//start game
                if (gameState == GameState.HELP)
                {
                    if (keyState.IsKeyDown(Keys.B))
                    {
                        gameState = GameState.START;
                    }
                }//game help

                //options
                if (gameState == GameState.OPTIONS)
                {
                    if (keyState.IsKeyDown(Keys.M))
                    {
                        mute = false;
                    }
                    if (keyState.IsKeyDown(Keys.B))
                    {
                        gameState = GameState.START;
                    }
                    if (graphics.IsFullScreen == false)
                    {
                        if (keyState.IsKeyDown(Keys.F))
                        {
                            graphics.IsFullScreen = true;
                            graphics.ApplyChanges();
                        }
                    }
                    else if (graphics.IsFullScreen == true)
                    {
                        if (keyState.IsKeyDown(Keys.F))
                        {
                            graphics.PreferredBackBufferWidth = 953;
                            graphics.PreferredBackBufferHeight = 580;
                            graphics.IsFullScreen = false;
                            graphics.ApplyChanges();
                        }
                    }
                }//options

                //game playing
                if (gameState == GameState.PLAYING) {
                    counter -= 0.01f;
                    if (joyState.DPad.Left == ButtonState.Pressed || keyState.IsKeyDown(Keys.Left))
                        {
                            rotation.Y += .05f; 
                            boundingSphere.Center = pos;
                        }
                        if (joyState.DPad.Right == ButtonState.Pressed || keyState.IsKeyDown(Keys.Right))
                        {
                            rotation.Y -= .05f;
                            boundingSphere.Center = pos;
                        }
                        if (joyState.Buttons.Y == ButtonState.Pressed || keyState.IsKeyDown(Keys.Up))
                        {
                            dx = velocity * (float)Math.Sin(rotation.Y);
                            dz = velocity * (float)Math.Cos(rotation.Y);
                            boundingSphere.Center = pos;

                            bulletPosition.X = velocity * (float)Math.Sin(rotation.Y);
                            bulletPosition.Z = velocity * (float)Math.Cos(rotation.Y);

                            for (int i = 0; i < objects.houses.Count;i++ )
                            {
                                if (boundingSphere.Intersects(objects.houses[i].boundingSphere))
                                {
                                    dx = 0;
                                    dz = 0;
                                }
                            }
                        }
                        else if (joyState.Buttons.A == ButtonState.Pressed || keyState.IsKeyDown(Keys.Down))
                        {
                            dx = -velocity * (float)Math.Sin(rotation.Y);
                            dz = -velocity * (float)Math.Cos(rotation.Y);

                            bulletPosition.X = -velocity * (float)Math.Sin(rotation.Y);
                            bulletPosition.Z = -velocity * (float)Math.Cos(rotation.Y);

                           boundingSphere.Center = pos;
                        }else {
                            dx = 0;
                            dz = 0;           
                        }
                        pos.X += dx;
                        pos.Z += dz;

                        cameraPos.X = pos.X;
                        cameraPos.Z = pos.Z;
            
                        lookAt = new Vector3(0,0,15);            
                        Quaternion q = Quaternion.CreateFromYawPitchRoll(rotation.Y,rotation.X,0);
                        Vector3 currentLookAt = Vector3.Transform(lookAt, q);

                        currentLookAt.X += cameraPos.X;
                        currentLookAt.Y += cameraPos.Y;
                        currentLookAt.Z += cameraPos.Z;

                       // cameraPos.X = pos.X-15*(float)Math.Sin(rotation.Y);
                      //  cameraPos.Z = pos.Z-15* (float)Math.Cos(rotation.Y);
                        view = Matrix.CreateLookAt(cameraPos, currentLookAt, Vector3.Up);


                    for (int i = 0; i < Robot.vertices.Length; i++)
                    {
                        Vector3 posR = new Vector3(Robot.vertices[i].Position.X, 0, Robot.vertices[i].Position.Z);
                        Vector3 posP = new Vector3(pos.X, 0, pos.Z);

                        Vector3 direction = Vector3.Normalize(posR - posP);
                        Robot.vertices[i].Position -= robotsSpeed * direction;

                        robot.boundingSphere.Center = Robot.vertices[0].Position;
                        robot2.boundingSphere.Center = Robot.vertices[1].Position;
                        robot3.boundingSphere.Center = Robot.vertices[2].Position;
                        robot4.boundingSphere.Center = Robot.vertices[3].Position;
                        robot5.boundingSphere.Center = Robot.vertices[4].Position;
                   
                        if (boundingSphere.Intersects(robot.boundingSphere) || boundingSphere.Intersects(robot2.boundingSphere) ||
                            boundingSphere.Intersects(robot3.boundingSphere) || boundingSphere.Intersects(robot4.boundingSphere)
                            || boundingSphere.Intersects(robot5.boundingSphere))
                        {
                                //gameState = GameState.GAMEOVER;
                               Robot.vertices[i].Position = new Vector3(-1000, 1, 330);
                               lives -= 1;
                         }
                         if(lives == 0){
                            gameState = GameState.GAMEOVER; // game over
                            if (mute == true)
                            {
                                game_over_sound.Play(0.5f, 0.0f, 0.0f);
                            }
                            else if (mute == false)
                            {
                                game_over_sound.Dispose();
                            }
                         }
                    }
                        
                   if (joyState.Triggers.Right == 1 || keyState.IsKeyDown(Keys.Space))
                    {
                        if (mute == true)
                        {
                            shooting.Play(0.1f, 0.0f, 0.0f);
                        }
                        else if (mute == false)
                        {
                            shooting.Dispose();
                        }

                        bulletPosition.X += bulletSpeed * (float)Math.Sin(rotation.Y);
                        bulletPosition.Z += bulletSpeed * (float)Math.Cos(rotation.Y);

                        boundingShpereBullet.Center = bulletPosition;
                        if (boundingShpereBullet.Intersects(robot.boundingSphere))
                        {                         
                            Robot.vertices[0].Position = new Vector3(-100, -100, -100); 
                            points += 20;
                            robot.boundingSphere.Radius = 0;
                        }
                        else if (boundingShpereBullet.Intersects(robot2.boundingSphere))
                        {
                            Robot.vertices[1].Position = new Vector3(-100, -100, -100);
                            points += 20;
                            robot2.boundingSphere.Radius = 0;
                        }
                        else if (boundingShpereBullet.Intersects(robot3.boundingSphere))
                        {
                            Robot.vertices[2].Position = new Vector3(-100, -100, -100);
                            points += 20;
                            robot3.boundingSphere.Radius = 0;
                        }
                        else if (boundingShpereBullet.Intersects(robot4.boundingSphere))
                        {
                            Robot.vertices[3].Position = new Vector3(-100, -100, -100); 
                            points += 20;
                            robot4.boundingSphere.Radius = 0;
                        }
                        else if (boundingShpereBullet.Intersects(robot5.boundingSphere))
                        {
                            Robot.vertices[4].Position = new Vector3(-100, -100, -100);
                            points += 20;
                            robot5.boundingSphere.Radius = 0;
                        }
                    }    
                    else
                    {
                        bulletPosition = new Vector3(pos.X - 7, pos.Y + 2, pos.Z - 8);
                    }
                   if (counter >= 0.00f)
                   {
                       if (points == 100)
                       {
                           gameState = GameState.SECONDLEVEL;
                           if (mute == true)
                           {
                               nextLevel.Play();
                           }
                           else if (mute == false)
                           {
                               nextLevel.Dispose();
                           }
                       }
                   }
                   else
                   {
                       gameState = GameState.GAMEOVER;
                   }
                   if (joyState.Triggers.Left == 1 || keyState.IsKeyDown(Keys.Escape))
                   {
                       if (mute == true)
                       {
                           pause.Play();
                       }
                       else if (mute == false)
                       {
                           pause.Dispose();
                       }
                       gameState = GameState.PAUSE;
                   }
                }
                   //second level
                if (gameState == GameState.SECONDLEVEL) {
                    if (keyState.IsKeyDown(Keys.Q))
                    {
                        this.Exit();
                    }                                        
                }//second level

                //pause
                if (gameState == GameState.PAUSE)
                {
                    if (joyState.Buttons.Start == ButtonState.Pressed || keyState.IsKeyDown(Keys.Enter))
                        {
                            robot.Update();
                            gameState = GameState.PLAYING;
                        }
                        else if (keyState.IsKeyDown(Keys.Q))
                        {
                            this.Exit();
                        }
                 }//pause
                if (gameState == GameState.GAMEOVER) {
                        if (keyState.IsKeyDown(Keys.Q))
                        {
                            this.Exit();
                        }
                }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            //start
            if (gameState == GameState.START)
            {
                graphics.GraphicsDevice.Clear(Color.Brown);
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

                spriteBatch.DrawString(lucidaConsole, "X Strike!", new Vector2(Window.ClientBounds.Width - (Window.ClientBounds.Width / 2+100), 10), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

                spriteBatch.DrawString(lucidaConsole, "Press Start or Enter to start", new Vector2(10, 220), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Press O for Option", new Vector2(10, 250), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Press H for Help", new Vector2(10, 280), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Press Q or Back to quit", new Vector2(10, 310), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

                spriteBatch.End();

                GraphicsDevice.BlendState = BlendState.Opaque;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            }//start
                //options
            else if(gameState == GameState.OPTIONS){
                graphics.GraphicsDevice.Clear(Color.Brown);
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

                spriteBatch.DrawString(lucidaConsole, "X Strike - Options", new Vector2(Window.ClientBounds.Width - (Window.ClientBounds.Width / 2 + 100), 10), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Press M for sound OFF", new Vector2(10, 220), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Press F to view fullscreen", new Vector2(10, 250), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Press B to return to main menu", new Vector2(10, 280), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

                spriteBatch.End();

                GraphicsDevice.BlendState = BlendState.Opaque;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;                
            } //options

                //playing
            else if (gameState == GameState.PLAYING)
            {
                     GraphicsDevice.Clear(Color.White);
                     bonesTransformation = new Matrix[playerModel.Bones.Count];
                     playerModel.CopyAbsoluteBoneTransformsTo(bonesTransformation);
                //player
                    foreach (ModelMesh mesh in playerModel.Meshes)
                    {
                        foreach (BasicEffect effect in mesh.Effects)
                        {
                            effect.World = bonesTransformation[mesh.ParentBone.Index] * Matrix.CreateScale(2f)*Matrix.CreateRotationX(0)
                                * Matrix.CreateRotationY(rotation.Y) * Matrix.CreateRotationZ(rotation.Z) * Matrix.CreateTranslation(pos);
                            effect.View = view;
                            effect.Projection = projection;
                            effect.EnableDefaultLighting();
                        }
                        mesh.Draw();
                    }
                //bullet
                    bonesTransformation = new Matrix[bulletModel.Bones.Count];
                    bulletModel.CopyAbsoluteBoneTransformsTo(bonesTransformation);
                    foreach (ModelMesh mesh in bulletModel.Meshes)
                    {
                        foreach (BasicEffect effect in mesh.Effects)
                        {
                            effect.World = bonesTransformation[mesh.ParentBone.Index] * Matrix.CreateScale(1f) * Matrix.CreateRotationX(bulletRotation.X)
                                * Matrix.CreateRotationY(bulletRotation.Y) * Matrix.CreateRotationZ(bulletRotation.Z) * Matrix.CreateTranslation(bulletPosition.X+15,bulletPosition.Y+2, bulletPosition.Z);
                            effect.View = view;
                            effect.Projection = projection;
                            effect.EnableDefaultLighting();
                        }
                        mesh.Draw();
                    }       
                roads.draw(projection, view);
                for (int h = 0; h < objects.houses.Count; h++) { objects.houses[h].draw(projection, view); }
                for (int t = 0; t < objects.trees.Count; t++) { objects.trees[t].draw(projection, view); }
               // for (int i = 0; i < robots.Count;i++ ){robots[i].draw(projection, player.view);}
                robot.draw(projection, view);
                robot2.draw(projection, view);
                robot3.draw(projection, view);
                robot4.draw(projection, view);
                robot5.draw(projection, view);

                objects.draw(projection, view, planeModel, new Vector3(-640,-10,350));

                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                spriteBatch.DrawString(lucidaConsole, points + " Points", new Vector2(10,10),Color.YellowGreen);
                spriteBatch.DrawString(lucidaConsole, "Level 1", new Vector2(Window.ClientBounds.Width - (Window.ClientBounds.Width / 2), 10), Color.YellowGreen);
                spriteBatch.DrawString(lucidaConsole, lives + "/3 " + "Lives", new Vector2(Window.ClientBounds.Width-110, 10), Color.Red);
                spriteBatch.DrawString(lucidaConsole, "Time Left: " + counter, new Vector2(10, Window.ClientBounds.Height - 50), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);

                spriteBatch.End();
                GraphicsDevice.BlendState = BlendState.Opaque;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap; 

            }//playing

            //game pause
            else if (gameState == GameState.PAUSE) {
                graphics.GraphicsDevice.Clear(Color.Transparent);
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                spriteBatch.DrawString(lucidaConsole, "X Strike - Pause", new Vector2(Window.ClientBounds.Width - (Window.ClientBounds.Width/2), 10), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

                spriteBatch.DrawString(lucidaConsole, "Press Enter to continue. . .", new Vector2(10, 250), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);               
                spriteBatch.DrawString(lucidaConsole, "Press Q to exit the game. . .", new Vector2(10, 280), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.End();

                GraphicsDevice.BlendState = BlendState.Opaque;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            }//pause

            //help
            else if (gameState == GameState.HELP)
            {
                graphics.GraphicsDevice.Clear(Color.Transparent);
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                spriteBatch.DrawString(lucidaConsole, "X Strike - Help", new Vector2(Window.ClientBounds.Width - (Window.ClientBounds.Width / 2), 10), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "How to Play", new Vector2(50, 50), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "The aim of the game is to kill all the robots to accumulate 100 points needed to progress to the next level", new Vector2(10, 80), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "to the next level.", new Vector2(10, 100), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Controls for keyboard", new Vector2(50, 125), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "LEFT and RIGHT arrows to change directions", new Vector2(10, 150), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "UP and DOWN arrows to move forwards and backwards", new Vector2(10, 170), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "SPACE for shooting", new Vector2(10, 190), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "ESCAPE for pause/options", new Vector2(10, 210), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Controls for controller", new Vector2(50, 235), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "LEFT and RIGHT arrows to change directions", new Vector2(10, 260), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Y and B arrows to move forwards and backwards", new Vector2(10, 280), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "RiGHT TRIGGER for shooting", new Vector2(10, 300), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "LEFT TRIGGER for pause/options", new Vector2(10, 320), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);  
                spriteBatch.DrawString(lucidaConsole, "Press B to return to main menu", new Vector2(10, Window.ClientBounds.Height - 50), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.End();

                GraphicsDevice.BlendState = BlendState.Opaque;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;            
            }//help

            //second level
            else if (gameState == GameState.SECONDLEVEL)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                spriteBatch.DrawString(lucidaConsole, "Congratulations, you reached the second level!", new Vector2(10, 250), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Press Q to Exit", new Vector2(10, Window.ClientBounds.Height - 50), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
   
                spriteBatch.End();
                GraphicsDevice.BlendState = BlendState.Opaque;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;             
            
            }//second level 
                //game over
            else if (gameState == GameState.GAMEOVER)
            {
                graphics.GraphicsDevice.Clear(Color.Transparent);
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                spriteBatch.DrawString(lucidaConsole, "X Strike!", new Vector2(Window.ClientBounds.Width - (Window.ClientBounds.Width / 2), 10), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

                spriteBatch.DrawString(lucidaConsole, "Game Over!", new Vector2(10, 250), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(lucidaConsole, "Press Q to Exit", new Vector2(10, Window.ClientBounds.Height - 50), Color.White, 0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

                spriteBatch.End();

                GraphicsDevice.BlendState = BlendState.Opaque;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;          
            }//game over

            base.Draw(gameTime);
        }
    }
}
