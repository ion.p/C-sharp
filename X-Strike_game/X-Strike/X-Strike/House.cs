﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace learning
{
    class House:Objects
    {
        Model model;
        Vector3 posHouse;
        Matrix[] bonesTransformation;
        public static VertexPositionTexture[] vertices;

        public House()
        {
            vertices = new VertexPositionTexture[12];

            vertices[0].Position = new Vector3(800, 3, 140);
            vertices[1].Position = new Vector3(400, 3, 140);
            vertices[2].Position = new Vector3(100, 3, 140);
            vertices[3].Position = new Vector3(800, 3, -320);
            vertices[4].Position = new Vector3(400, 3, -320);
            vertices[5].Position = new Vector3(100, 3, -320);
            vertices[6].Position = new Vector3(-1450, 3, 140);
            vertices[7].Position = new Vector3(-1000, 3, 140);
            vertices[8].Position = new Vector3(-600, 3, 140);
            vertices[9].Position = new Vector3(-1450, 3, -320);
            vertices[10].Position = new Vector3(-1000, 3, -320);
            vertices[11].Position = new Vector3(-600, 3, -320);
            for (int h = 0; h < vertices.Length;h++ )
            {
                boundingSphere.Radius = 20f;
                boundingSphere.Center = vertices[h].Position;
            }
        }

        public void loadContent(ContentManager content)
        {
            model = content.Load<Model>("Bambo_House");            
        }
        public void draw(Matrix projection, Matrix view) {
            for (int h = 0; h < vertices.Length;h++ )
            {
                bonesTransformation = new Matrix[model.Bones.Count];
                model.CopyAbsoluteBoneTransformsTo(bonesTransformation);
                foreach (ModelMesh mesh in model.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.TextureEnabled = false;
                        effect.World = bonesTransformation[mesh.ParentBone.Index] * Matrix.CreateScale(1f) * Matrix.CreateTranslation(vertices[h].Position);
                        effect.View = view;
                        effect.Projection = projection;
                        effect.EnableDefaultLighting();
                    }
                    mesh.Draw();
                }
            }
        }
    }
}
